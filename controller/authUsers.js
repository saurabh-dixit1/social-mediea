const nodemailer = require('nodemailer');
const crypto = require('crypto');
const User = require('../models/user');
const PasswordReset = require('../models/PasswordReset');


//form render
module.exports.showForgotPassword = async (req, res) => {
    return res.render('forgotPassword',{
        title: 'Forgot Password Page | My Book',
        message: {typs:null ,text:null}

    });
  };

// Transporter
const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
    user: 'smartds2550',
    pass: '2222',
    },
});


// forgot password controller
exports.forgotPassword = async (req, res) => {
  const { email } = req.body;

  // Find the user associated with the email
  const user = await User.findOne({ email });

  if (!user) {
    return res.render('forgotPassword', { 
        title:'Forgot Passwor Page | My Book',
        message: { type: 'danger', text: 'No user found with that email address.' }});
  }

  // Generate a password reset token
  const token = crypto.randomBytes(20).toString('hex');
  const passwordReset = new PasswordReset({
    user: user._id,
    token,
    createdAt: new Date(),
  });
  await passwordReset.save();

  // Construct the password reset email
  const mailOptions = {
    from: 'smartds2550@gmail.com',
    to: user.email,
    subject: 'Password Reset Request',
    html: `Click the link below to reset your password:<br><br><a href="${req.protocol}://${req.get('host')}/reset-password/${token}">${req.protocol}://${req.get('host')}/reset-password/${token}</a>`,
  };

  // Send the email
  transporter.sendMail(mailOptions, (err) => {
    if (err) {
      console.log(err);
      res.render('forgotPassword', {
         title:'Forgot Password Page | My Book',
         message: { type: 'danger', text: 'There was an error sending the password reset email. Please try again later.' }});
    } else {
      res.render('forgotPassword', {
         title:'Forgot Password Page | My Book',
         message: { type: 'success', text: 'A password reset link has been sent to your email address.' }});
    }
  });
};

//password link handelor
exports.handleResetPasswordLink = async (req, res) => {
  const { token } = req.params;

  // Look up the password reset token in the database
  const passwordReset = await PasswordReset.findOne({ token });

  // If the token doesn't exist, display an error message
  if (!passwordReset) {
    return res.render('resetPassword', { 
        title:'Forgot Password Final Page | My Book',
        message: { type: 'danger', text: 'Invalid password reset link. Please request a new link.' }});
  }

  // If the token has expired, display an error message
  if (new Date() - passwordReset.createdAt > 30 * 60 * 1000) {
    await passwordReset.remove();
    return res.render('resetPassword', {
         title:'Forgot Password Final Page | My Book',
         message: { type: 'danger', text: 'Password reset link has expired. Please request a new link.' }});
  }

  // Render the password reset form
  res.render('resetPassword', {
    title:'Forgot Password Final Page | My Book',
     token 
    });
};

// reset password updates
exports.resetPassword = async (req, res) => {
    const { token } = req.params;
    const { password } = req.body.password;

    // Look up the password reset token in the database
    const passwordReset = await PasswordReset.findOne({ token });

    // If the token doesn't exist, display an error message
    if (!passwordReset) {
        return res.render('resetPassword', {
            title:'Forgot Password Final Page | My Book',  
            message: { type: 'danger', text: 'Invalid password reset link. Please request a new link.' }
        });
    }
    // If the token has expired, display an error message
    if (new Date() - passwordReset.createdAt > 30 * 60 * 1000) {
        await passwordReset.remove();
        return res.render('resetPassword', {
            title:'Forgot Password Final Page | My Book',
            message: { type: 'danger', text: 'Password reset link has expired. Please request a new link.' }
        });
    }
    // Look up the user associated with the password reset token
    const user = await User.findById(passwordReset.userId);

    // Update the user's password and remove the password reset token
    user.password = password;
    await user.save();
    await passwordReset.remove();

    // Redirect the user to the login page with a success message
    res.redirect('/user/sign-in', {
        title:'Sign-in Page | My Book',
        message: { type: 'success', text: 'Your password has been reset. Please log in with your new password.' }
    });
};