const express = require('express');
const router = express.Router();
const passwordResetController = require('../controller/authUsers');



// Route to send a password reset email
router.post('/forgot-password', passwordResetController.forgotPassword);
router.get('/show-forgot-password', passwordResetController.showForgotPassword);

// Route to handle a password reset link
router.get('/reset-password/:token', passwordResetController.handleResetPasswordLink);

// Route to handle a password reset form submission
router.post('/reset-password/:token', passwordResetController.resetPassword);

module.exports = router;