const passport = require('passport');
const GitHubStrategy = require('passport-github').Strategy;
const enviroment = require('./enviroment');

const crypto = require('crypto');
const User = require('../models/user');

passport.use(new GitHubStrategy({
    clientID: enviroment.githubClientID,
    clientSecret: enviroment.githubClientSecret,
    callbackURL: enviroment.gitubCallbackURL
  },
    function(accessToken, refreshToken, profile, cb) {
        User.findOne({email: profile.emails[0].value}).exec(function(error , user){
            if(error){
                console.log('error in Github-stretgy',error); 
                return;
            }
            if(user){
                return done(null , user);
            }else{
                User.create({
                    name: profile.displayName,
                    email: profile.emails[0].value,
                    password: crypto.randomBytes(80).toString('hex')

                },function(error , user){
                    if(error){
                        console.log('error in creating user with Github-stretgy',error); 
                        return;
                    }
                    return done(null , user);
                }); 
            }
        });
    }
));

module.exports = passport;