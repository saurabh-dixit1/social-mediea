// select all like buttons
const likeButtons = document.querySelectorAll('.like-button');

// add click event listener to each button
  likeButtons.forEach(button => {
    button.addEventListener('click', async () => {
    const id = button.getAttribute('data-id');
    const type = button.getAttribute('data-type');
    const url = `/likes/toggle/${type}/${id}`;

try {
  const response = await fetch(url, { method: 'POST' });
  const result = await response.json();

  if (result.success) {
    const countElement = button.nextElementSibling;
    const count = parseInt(countElement.innerText.split(' ')[0]);
    if (result.action === 'like') {
    countElement.innerText = `${count + 1} Likes`;
    button.innerText = 'Unlike';
    }else {
      countElement.innerText = `${count - 1} Likes`;
      button.innerText = 'Like';
    }   
  }
} catch (error) {
    console.error(error);
    }
  });
});
const fileInput = document.getElementById("fileInput");
const fileLabel = document.getElementById("fileLabel");
const fileName = document.getElementById("fileName");

fileLabel.addEventListener("click", function() {
  fileInput.click();
});

fileInput.addEventListener("change", function() {
  if (fileInput.value) {
    const name = fileInput.value.split("\\").pop();
    fileName.innerText = name;
  } else {
    fileName.innerText = "No file selected.";
  }
});

